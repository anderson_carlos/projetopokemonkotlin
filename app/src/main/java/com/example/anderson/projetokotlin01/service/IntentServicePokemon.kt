package com.example.anderson.projetokotlin01.service

import android.app.IntentService
import android.content.Intent
import android.content.Context
import com.example.anderson.projetokotlin01.basica.Pokemon
import com.example.anderson.projetokotlin01.broadcast.ReceiverNotification


class IntentServicePokemon : IntentService("IntentServicePokemon") {

    var pokemon : Pokemon? = null

    override fun onHandleIntent(intent: Intent?) {

        pokemon = intent?.getSerializableExtra("pokemon") as Pokemon?

        if(pokemon != null) {
            val intentService = Intent(this, ReceiverNotification::class.java)
            intentService.putExtra("pokemon", pokemon)
            sendBroadcast(intentService)
        }

    }
}
