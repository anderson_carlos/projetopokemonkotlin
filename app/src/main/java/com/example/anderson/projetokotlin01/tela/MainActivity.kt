package com.example.anderson.projetokotlin01.tela


import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.adapter.AdapterListarPokemons
import com.example.anderson.projetokotlin01.adapter.AdapterRecyclerViewPokemons
import com.example.anderson.projetokotlin01.basica.Pokemon
import com.example.anderson.projetokotlin01.service.IntentServicePokemon
import com.example.anderson.projetokotlin01.service.IntentServiceQtdPokemon
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    companion object {
        var mListaDePokemons = ArrayList<Pokemon>()
    }

    var pokemon: Pokemon? = null
    val adapter = AdapterListarPokemons(this, mListaDePokemons)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mockDadosPokemon()

        // Fluxo com ListView
        //listViewPokemon.adapter = adapter
        //verificarPokemonAlarme()


        // Fluxo com RecyclerView
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewPokemon.layoutManager = layoutManager
        recyclerViewPokemon.adapter = AdapterRecyclerViewPokemons(this, mListaDePokemons)


        /*  listViewPokemon.setOnItemClickListener { parent, view, position, id ->

              val intent = Intent(this, DetalheActivity::class.java)
              pokemon = mListaDePokemons.get(position)
              intent.putExtra("pokemon", pokemon)
              intent.putExtra("position", position)
              startActivityForResult(intent, 1)
          }*/


    }

    private fun verificarPokemonAlarme() {
        val intentServiceQtdPokemon = Intent(this, IntentServiceQtdPokemon::class.java)

        //Inicio AlarmManager
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pending = PendingIntent.getService(this, 0, intentServiceQtdPokemon, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 10000, pending)

    }

    fun mockDadosPokemon() {
        if (mListaDePokemons.size == 0) {
            mListaDePokemons.add(Pokemon("?", getString(R.string.eletrico), R.mipmap.ic_pikachu_sombra, "Pikachu", false))
            mListaDePokemons.add(Pokemon("?", getString(R.string.agua), R.mipmap.ic_squirtle_sombra, "Squirtle", false))
            mListaDePokemons.add(Pokemon("?", getString(R.string.fogo), R.mipmap.ic_charmander_sombra, "Charmander", false))
            mListaDePokemons.add(Pokemon("?", getString(R.string.psiquico), R.mipmap.ic_mew_sombra, "Mew", false))
            mListaDePokemons.add(Pokemon("?", getString(R.string.normal), R.mipmap.ic_snorlax_sombra, "Snorlax", false))
            mListaDePokemons.add(Pokemon("?", getString(R.string.agua), R.mipmap.ic_gyarados_sombra, "Gyarados", false))
            mListaDePokemons.add(Pokemon("?", getString(R.string.fantasma), R.mipmap.ic_gengar_sombra, "Gengar", false))
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK) {
                pokemon = data?.getSerializableExtra("objetoPokemon") as Pokemon?
                var posicao = data?.getIntExtra("position", -1)


                mListaDePokemons[posicao!!] = pokemon!!

                adapter.notifyDataSetChanged()

                if (pokemon?.acertou!!) {

                    Toast.makeText(this, R.string.msg_sucesso, Toast.LENGTH_SHORT).show()

                    val intentService = Intent(this, IntentServicePokemon::class.java)
                    intentService.putExtra("pokemon", pokemon)
                    startService(intentService)

                } else {
                    Toast.makeText(this, R.string.msg_erro, Toast.LENGTH_SHORT).show()

                }

            }

        }
    }

}
