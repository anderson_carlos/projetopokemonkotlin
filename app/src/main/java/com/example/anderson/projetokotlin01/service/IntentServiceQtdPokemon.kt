package com.example.anderson.projetokotlin01.service

import android.app.IntentService
import android.content.Intent
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.notification.NotificationUtil
import com.example.anderson.projetokotlin01.tela.MainActivity

class IntentServiceQtdPokemon : IntentService("IntentServiceQtdPokemon") {


    override fun onHandleIntent(intent: Intent?) {
        var count = 0
        for (pokemon in MainActivity.mListaDePokemons) {

            if (!pokemon.acertou) {
                count++
            }

        }
        val intentPokemon = Intent(this, MainActivity::class.java)
        NotificationUtil.create(this, 0, intentPokemon, getString(R.string.informacao), getString(R.string.ainda_faltam)+" "+  count.toString() +" "+ getString(R.string.pokemons))


    }


}
