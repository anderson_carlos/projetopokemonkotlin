package com.example.anderson.projetokotlin01.tela

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.fragment.PokemonDetalheFragment

class DetalheActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe)

        val fragment = PokemonDetalheFragment()
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentActivityDetalhe, fragment)
        transaction.commit()


    }
}
