package com.example.anderson.projetokotlin01.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.example.anderson.projetokotlin01.R

/**
 * Created by Anderson on 01/07/2018.
 */
object NotificationUtil {


    fun create(context: Context, id: Int, intent: Intent, title: String, text: String){

        //Responsável por capturar o servico de notificacao do android
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        //for android 8
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var channel = NotificationChannel("NotificationAndroid8","name", NotificationManager.IMPORTANCE_HIGH)
            manager.createNotificationChannel(channel)
        }

        val pendingIntent = PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)

        //definir as configuracoes do notification
        val builder = NotificationCompat.Builder(context,"id")
                .setContentIntent(pendingIntent)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.mipmap.ic_pokebola)
                .setAutoCancel(true)

        //Objeto notification
        val notification = builder.build()

        //Starta a notificacao
        manager.notify(id, notification)
    }

}