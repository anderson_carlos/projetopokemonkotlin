package com.example.anderson.projetokotlin01.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.anderson.projetokotlin01.basica.Pokemon


import kotlinx.android.synthetic.main.item_lista_pokemon.view.*

/**
 * Created by Anderson on 23/07/2018.
 */
class ViewHolderListPokemons(itemView : View) : RecyclerView.ViewHolder(itemView) {

    fun bindView(pokemon: Pokemon) {

        val imageViewPokemon = itemView.imageViewPokemon
        val txtNomePokemon = itemView.txtNomePokemon
        val txtTipoPokemon = itemView.txtPokemonTipo

        imageViewPokemon.setImageResource(pokemon.idImagePokemon)
        txtNomePokemon.text = pokemon.nome
        txtTipoPokemon.text = pokemon.tipo

    }

}