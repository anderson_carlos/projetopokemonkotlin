package com.example.anderson.projetokotlin01.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.basica.Pokemon
import com.example.anderson.projetokotlin01.notification.NotificationUtil
import com.example.anderson.projetokotlin01.tela.DetalheActivity


class ReceiverNotification : BroadcastReceiver() {

 var pokemon : Pokemon? = null
    override fun onReceive(context: Context, intent: Intent) {

        pokemon = intent.getSerializableExtra("pokemon") as Pokemon?

        if(pokemon != null) {
            val intentDetalhe = Intent(context, DetalheActivity::class.java)
            intentDetalhe.putExtra("pokemon", pokemon)

            NotificationUtil.create(context, 1, intentDetalhe, context.getString(R.string.msg_sucesso), context.getString(R.string.inicio_frase)+ " "+ pokemon?.nome)
        }
    }
}
