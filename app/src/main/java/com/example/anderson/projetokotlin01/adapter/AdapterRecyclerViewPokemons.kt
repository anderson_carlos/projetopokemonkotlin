package com.example.anderson.projetokotlin01.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.basica.Pokemon
import com.example.anderson.projetokotlin01.viewholder.ViewHolderListPokemons

/**
 * Created by Anderson on 23/07/2018.
 */
class AdapterRecyclerViewPokemons(var ctx: Context, var mListaPokemon: ArrayList<Pokemon>) : RecyclerView.Adapter<ViewHolderListPokemons>() {


    // Metodo responsavel por inflar meu layout e retornar a instancia da minha viewHolder.
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolderListPokemons {
        val view : View = LayoutInflater.from(ctx).inflate(R.layout.item_lista_pokemon, parent, false)
        // retorna a instancia da minha ViewHolder
        return ViewHolderListPokemons(view)
    }

    override fun onBindViewHolder(holder: ViewHolderListPokemons?, position: Int) {
        val pokemon= mListaPokemon[position]

        // A funcao "let" valida um bloco de codigo se é null, ou seja nao precisa colocar "?" em toda parte do codigo
       holder?.let {
           holder.bindView(pokemon)
        }

    }

    override fun getItemCount(): Int {
        return mListaPokemon.size
    }
}